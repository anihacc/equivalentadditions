package tk.zeitheron.equivadditions.blocks;

import java.util.List;

import javax.annotation.Nonnull;

import tk.zeitheron.equivadditions.tiles.TileCustomCollector;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;

import moze_intel.projecte.api.item.IItemEmc;
import moze_intel.projecte.gameObjs.blocks.BlockDirection;
import moze_intel.projecte.gameObjs.tiles.CollectorMK1Tile;
import moze_intel.projecte.utils.MathUtils;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class BlockCustomCollector extends BlockDirection implements ITileBlock<TileCustomCollector>
{
	public final int capacity, generation, mk;
	
	public BlockCustomCollector(int capacity, int generation, int mk)
	{
		super(Material.GLASS);
		this.mk = mk;
		this.capacity = capacity;
		this.generation = generation;
		setTranslationKey("collector_mk" + mk);
		setLightLevel(1F);
		setHardness(0.3f);
	}
	
	@Override
	public Class<TileCustomCollector> getTileClass()
	{
		return TileCustomCollector.class;
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		GuiManager.openGui(player, WorldUtil.cast(world.getTileEntity(pos), TileSyncable.class));
		return true;
	}
	
	@Override
	public boolean hasTileEntity(IBlockState state)
	{
		return true;
	}
	
	@Override
	public TileEntity createTileEntity(@Nonnull World world, @Nonnull IBlockState state)
	{
		return new TileCustomCollector(capacity, generation);
	}
	
	@Override
	public boolean hasComparatorInputOverride(IBlockState state)
	{
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(IBlockState state, World world, BlockPos pos)
	{
		TileCustomCollector tile = ((TileCustomCollector) world.getTileEntity(pos));
		ItemStack charging = tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.UP).getStackInSlot(CollectorMK1Tile.UPGRADING_SLOT);
		if(!charging.isEmpty())
		{
			if(charging.getItem() instanceof IItemEmc)
			{
				IItemEmc itemEmc = ((IItemEmc) charging.getItem());
				double max = itemEmc.getMaximumEmc(charging);
				double current = itemEmc.getStoredEmc(charging);
				return MathUtils.scaleToRedstone((long) current, (long) max);
			} else
			{
				double needed = tile.getEmcToNextGoal();
				double current = tile.getStoredEmc();
				return MathUtils.scaleToRedstone((long) current, (long) needed);
			}
		} else
		{
			return MathUtils.scaleToRedstone((long) tile.getStoredEmc(), (long) tile.getMaximumEmc());
		}
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn)
	{
		String unit = I18n.format("pe.emc.name");
		String rate = I18n.format("pe.emc.rate");
		
		tooltip.add(TextFormatting.DARK_PURPLE + String.format(I18n.format("pe.emc.maxgenrate_tooltip") + TextFormatting.BLUE + " %d " + rate, generation));
		tooltip.add(TextFormatting.DARK_PURPLE + String.format(I18n.format("pe.emc.maxstorage_tooltip") + TextFormatting.BLUE + " %,d " + unit, capacity));
	}
	
	@Override
	public boolean isSideSolid(IBlockState state, @Nonnull IBlockAccess world, @Nonnull BlockPos pos, EnumFacing side)
	{
		return true;
	}
	
	@Override
	public void breakBlock(@Nonnull World world, @Nonnull BlockPos pos, @Nonnull IBlockState state)
	{
		TileEntity ent = world.getTileEntity(pos);
		
		if(ent != null)
		{
			IItemHandler handler = ent.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing.UP);
			for(int i = 0; i < handler.getSlots(); i++)
				if(i != CollectorMK1Tile.LOCK_SLOT && !handler.getStackInSlot(i).isEmpty())
					InventoryHelper.spawnItemStack(world, pos.getX(), pos.getY(), pos.getZ(), handler.getStackInSlot(i));
		}
		
		super.breakBlock(world, pos, state);
	}
}