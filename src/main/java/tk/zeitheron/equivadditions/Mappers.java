package tk.zeitheron.equivadditions;

import java.util.function.Function;

import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;

public class Mappers
{
	public static <K, V> IndexedMap<K, V> withKeys(Iterable<V> values, Function<V, K> keySupplier)
	{
		IndexedMap<K, V> mp = new IndexedMap<>();
		for(V v : values)
			mp.put(keySupplier.apply(v), v);
		return mp;
	}
	
	public static <K, V> IndexedMap<K, V> withValues(Iterable<K> keys, Function<K, V> valSupplier)
	{
		IndexedMap<K, V> mp = new IndexedMap<>();
		for(K k : keys)
			mp.put(k, valSupplier.apply(k));
		return mp;
	}
}