package tk.zeitheron.equivadditions.init;

import tk.zeitheron.equivadditions.InfoEA;
import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;

@RegisterRecipes(modid = InfoEA.MOD_ID)
public class RecipesEA extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		IForgeRegistry<Item> items = ForgeRegistries.ITEMS;
		
		Item item_pe_matter = items.getValue(new ResourceLocation("projecte", "item.pe_matter"));
		Item item_pe_fuel = items.getValue(new ResourceLocation("projecte", "item.pe_fuel"));
		Item item_pe_klein_star = items.getValue(new ResourceLocation("projecte", "item.pe_klein_star"));
		Item matter_block = items.getValue(new ResourceLocation("projecte", "matter_block"));
		
		ItemStack alchemicalCoal = new ItemStack(item_pe_fuel, 1, 0);
		ItemStack mobiusFuel = new ItemStack(item_pe_fuel, 1, 1);
		ItemStack aeternalisFuel = new ItemStack(item_pe_fuel, 1, 2);
		
		ItemStack darkMatter = new ItemStack(item_pe_matter, 1, 0);
		ItemStack redMatter = new ItemStack(item_pe_matter, 1, 1);
		
		shaped(new ItemStack(BlocksEA.PIPE_EMC_MK1, 8), "ggg", "rdr", "ggg", 'g', "glowstone", 'r', alchemicalCoal.copy(), 'd', "blockDiamond");
		shaped(new ItemStack(BlocksEA.PIPE_EMC_MK2, 8), "ggg", "rdr", "ggg", 'g', "glowstone", 'r', mobiusFuel.copy(), 'd', darkMatter.copy());
		shaped(new ItemStack(BlocksEA.PIPE_EMC_MK3, 8), "ggg", "rdr", "ggg", 'g', "glowstone", 'r', aeternalisFuel.copy(), 'd', redMatter.copy());
		shaped(new ItemStack(BlocksEA.PIPE_EMC_MK4, 8), "ggg", "rdr", "ggg", 'g', "glowstone", 'r', new ItemStack(ItemsEA.ZEITHERON_FUEL), 'd', new ItemStack(ItemsEA.BLUE_MATTER));
		
		shaped(new ItemStack(ItemsEA.BLUE_MATTER), "fff", "mmm", "fff", 'f', new ItemStack(ItemsEA.ZEITHERON_FUEL), 'm', redMatter.copy());
		shaped(new ItemStack(ItemsEA.BLUE_MATTER), "fmf", "fmf", "fmf", 'f', new ItemStack(ItemsEA.ZEITHERON_FUEL), 'm', redMatter.copy());
		
		shapeless(new ItemStack(ItemsEA.ZEITHERON_FUEL), new ItemStack(items.getValue(new ResourceLocation("projecte", "item.pe_philosophers_stone"))), aeternalisFuel.copy(), aeternalisFuel.copy(), aeternalisFuel.copy(), aeternalisFuel.copy());
		shapeless(new ItemStack(ItemsEA.ZEITHERON_FUEL, 9), new ItemStack(BlocksEA.ZEITHERON_FUEL_BLOCK));
		shaped(BlocksEA.ZEITHERON_FUEL_BLOCK, "zzz", "zzz", "zzz", 'z', new ItemStack(ItemsEA.ZEITHERON_FUEL));
		
		shaped(BlocksEA.ENERGY_COLLECTOR_MK4, "gmg", "gcg", "ggg", 'g', "glowstone", 'c', new ItemStack(items.getValue(new ResourceLocation("projecte", "collector_mk3"))), 'm', new ItemStack(ItemsEA.BLUE_MATTER));
		
		shapeless(new ItemStack(ItemsEA.BLUE_MATTER, 4), new ItemStack(BlocksEA.BLUE_MATTER_BLOCK));
		shaped(BlocksEA.BLUE_MATTER_BLOCK, "mm", "mm", 'm', new ItemStack(ItemsEA.BLUE_MATTER));
		
		shaped(BlocksEA.EMC_PROXY, "bkb", "kmk", "bkb", 'b', new ItemStack(matter_block), 'k', new ItemStack(item_pe_klein_star, 1, 4), 'm', BlocksEA.BLUE_MATTER_BLOCK);
		
		ItemStack aeternalisFuelx4 = aeternalisFuel.copy();
		aeternalisFuelx4.setCount(4);
		
		shapeless(aeternalisFuelx4.copy(), new ItemStack(items.getValue(new ResourceLocation("projecte", "item.pe_philosophers_stone"))), new ItemStack(ItemsEA.ZEITHERON_FUEL));
	}
	
	@Override
	protected String getMod()
	{
		return "hammercore";
	}
}