package tk.zeitheron.equivadditions.init;

import net.minecraft.item.Item;

public class ItemsEA
{
	public static final Item BLUE_MATTER = new Item().setTranslationKey("blue_matter");
	public static final Item ZEITHERON_FUEL = new Item().setTranslationKey("zeitheron_fuel");
}