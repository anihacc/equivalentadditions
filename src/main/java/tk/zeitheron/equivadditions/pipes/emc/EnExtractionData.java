package tk.zeitheron.equivadditions.pipes.emc;

import java.util.HashMap;

import net.minecraft.util.math.BlockPos;

class EnExtractionData extends HashMap<BlockPos, Double>
{
	static final ThreadLocal<EnExtractionData> DATAS = ThreadLocal.withInitial(EnExtractionData::new);

	public static EnExtractionData get()
	{
		return DATAS.get();
	}
}