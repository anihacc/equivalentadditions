package tk.zeitheron.equivadditions.pipes.emc.impl;

import tk.zeitheron.equivadditions.pipes.emc.EMCPipe;
import tk.zeitheron.equivadditions.tiles.TileEMCPipe;

import net.minecraft.util.ResourceLocation;

public class SimpleEMCPipe extends EMCPipe
{
	public final int cap;
	public final ResourceLocation tex;
	
	public SimpleEMCPipe(TileEMCPipe tile, ResourceLocation tex, int capacity)
	{
		super(tile);
		this.tex = tex;
		this.cap = capacity;
	}

	@Override
	public ResourceLocation getPipeTexture()
	{
		return tex;
	}

	@Override
	public int getMaxTransfer()
	{
		return cap;
	}
}