package tk.zeitheron.equivadditions.pipes.emc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import tk.zeitheron.equivadditions.Mappers;
import tk.zeitheron.equivadditions.tiles.TileEMCPipe;
import com.zeitheron.hammercore.lib.zlib.utils.IndexedMap;

import moze_intel.projecte.api.tile.IEmcAcceptor;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class EMCPipeGrid
{
	public final GridAccessPoint gridAP;
	public final List<BlockPos> positions = new ArrayList<>();
	public EMCPipe master;
	public World world;
	
	public EMCPipeGrid parent;
	
	public EMCPipeGrid(World world, EMCPipe master)
	{
		gridAP = new GridAccessPoint(world, master.tile.getPos());
		this.world = world;
		this.master = master;
		onMajorGridChange();
	}
	
	public boolean isMaster(BlockPos pos)
	{
		return parent != null ? parent.isMaster(pos) : Objects.equals(pos, this.master.tile.getPos());
	}
	
	public static EMCPipeGrid merge(EMCPipeGrid a, EMCPipeGrid b)
	{
		EMCPipeGrid newGrid = new EMCPipeGrid(a.world, a.master);
		
		Stream.concat(a.stream(), b.stream()).forEach(p ->
		{
			p.grid = null;
			newGrid.addPipe(p);
		});
		
		return newGrid;
	}
	
	/**
	 * May merge two grids.
	 */
	public void addPipe(EMCPipe pipe)
	{
		EMCPipeGrid grid = pipe.grid != null ? merge(this, pipe.grid) : this;
		grid.positions.add(pipe.tile.getPos());
		pipe.grid = grid;
		onMajorGridChange();
	}
	
	public void setParent(EMCPipeGrid parent)
	{
		this.parent = parent;
	}
	
	public void onMajorGridChange()
	{
		if(parent != null)
		{
			while(!positions.isEmpty())
			{
				BlockPos p = positions.remove(0);
				EMCPipe pipe = TileEMCPipe.getPipe(world, p);
				if(pipe != null)
				{
					pipe.grid = parent;
					if(!parent.positions.contains(p))
						parent.positions.add(p);
				}
			}
			return;
		}
		
		forEachPipe(pipe ->
		{
			if(pipe != null)
				pipe.grid = null;
		});
		
		positions.clear();
		
		positions.add(this.master.tile.getPos());
		
		for(int i = 0; i < positions.size(); ++i)
		{
			EMCPipe pipe = TileEMCPipe.getPipe(world, positions.get(i));
			if(pipe == null)
				positions.remove(i);
			else
			{
				pipe.grid = this;
				for(EnumFacing face : EnumFacing.VALUES)
				{
					EMCPipe ep = pipe.tile.getRelPipe(face);
					if(ep != null && !positions.contains(ep.tile.getPos()))
						positions.add(ep.tile.getPos());
				}
			}
		}
		
		balance();
	}
	
	public void forEachPipe(Consumer<EMCPipe> consumer)
	{
		stream().forEach(consumer::accept);
	}
	
	public Stream<EMCPipe> stream()
	{
		return positions.stream().map(pos -> TileEMCPipe.getPipe(world, pos)).filter(p -> p != null);
	}
	
	public void balance()
	{
		long emc = 0;
		int count = 0;
		for(int i = 0; i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			EMCPipe pipe = TileEMCPipe.getPipe(world, pos);
			if(pipe != null)
			{
				if(pipe.isValid())
				{
					double en = pipe.getBuffer().getEnergyStored();
					emc += en;
					
					++count;
				} else
				{
					positions.remove(i);
					pipe.grid = null;
					onMajorGridChange();
					
					return;
				}
			}
		}
		if(count == 0)
			return;
		long epp = emc / count;
		for(int i = 0; i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			EMCPipe pipe = TileEMCPipe.getPipe(world, pos);
			if(pipe != null)
				pipe.setEnergy(epp);
		}
		long rest = emc - epp * count;
		for(int i = 0; rest > 0 && i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			EMCPipe pipe = TileEMCPipe.getPipe(world, pos);
			if(pipe != null && pipe.isValid())
				rest -= pipe.getBuffer().acceptEMC(null, rest);
		}
	}
	
	private class GridAccessPoint extends EMCAccessPoint
	{
		public GridAccessPoint(World world, BlockPos pos)
		{
			super(world, pos);
		}
		
		public PipeEMCStorage getPipeBuf(World world, BlockPos pos)
		{
			EMCPipe pipe = TileEMCPipe.getPipe(world, pos);
			if(pipe != null)
				return pipe.getBuffer();
			return null;
		}
		
		@Override
		public IndexedMap<EnumFacing, IEmcAcceptor> findEMCAcceptors()
		{
			return Mappers.withKeys(positions.stream().map(pos -> getPipeBuf(world, pos)).filter(es -> es != null).collect(Collectors.toList()), k -> EnumFacing.DOWN);
		}
		
		@Override
		public List<IFluidHandler> findFLAcceptors(FluidStack fluid)
		{
			return Collections.emptyList();
		}
	}
}