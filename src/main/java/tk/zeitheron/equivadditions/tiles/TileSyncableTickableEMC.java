package tk.zeitheron.equivadditions.tiles;

import java.util.Map;

import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import tk.zeitheron.equivadditions.api.IRelay;
import com.zeitheron.hammercore.tile.TileSyncableTickable;

import moze_intel.projecte.api.tile.IEmcAcceptor;
import moze_intel.projecte.api.tile.IEmcProvider;
import moze_intel.projecte.api.tile.IEmcStorage;
import moze_intel.projecte.gameObjs.tiles.RelayMK1Tile;
import moze_intel.projecte.utils.WorldHelper;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.items.ItemStackHandler;

public class TileSyncableTickableEMC extends TileSyncableTickable implements IEmcStorage
{
	protected long maximumEMC;
	protected long currentEMC = 0;
	
	protected TileSyncableTickableEMC()
	{
		setMaximumEMC(Long.MAX_VALUE);
	}
	
	public TileSyncableTickableEMC(long maxAmount)
	{
		setMaximumEMC(maxAmount);
	}
	
	/**
	 * The amount provided will be divided and evenly distributed as best as
	 * possible between adjacent IEMCAcceptors Remainder or rejected EMC is
	 * added back to this provider
	 *
	 * @param emc
	 *            The maximum combined emc to send to others
	 */
	protected void sendToAllAcceptors(long emc)
	{
		if(!(this instanceof IEmcProvider))
		{
			// todo move this method somewhere
			throw new UnsupportedOperationException("sending without being a provider");
		}
		
		Map<EnumFacing, TileEntity> tiles = Maps.filterValues(WorldHelper.getAdjacentTileEntitiesMapped(world, this), Predicates.instanceOf(IEmcAcceptor.class));
		
		if(!tiles.isEmpty())
		{
			long emcPer = emc / tiles.size();
			for(Map.Entry<EnumFacing, TileEntity> entry : tiles.entrySet())
			{
				if(this instanceof IRelay && (entry.getValue() instanceof RelayMK1Tile || entry.getValue() instanceof IRelay))
					continue;
				long provide = ((IEmcProvider) this).provideEMC(entry.getKey().getOpposite(), emcPer);
				long remain = provide - ((IEmcAcceptor) entry.getValue()).acceptEMC(entry.getKey(), provide);
				this.addEMC(remain);
			}
		}
	}
	
	public final void setMaximumEMC(long max)
	{
		maximumEMC = max;
		if(currentEMC > maximumEMC)
			currentEMC = maximumEMC;
	}
	
	@Override
	public long getStoredEmc()
	{
		return currentEMC;
	}
	
	@Override
	public long getMaximumEmc()
	{
		return maximumEMC;
	}
	
	/**
	 * Add EMC directly into the internal buffer. Use for internal
	 * implementation of your tile
	 */
	protected void addEMC(long toAdd)
	{
		currentEMC += toAdd;
		if(currentEMC > maximumEMC)
			currentEMC = maximumEMC;
	}
	
	/**
	 * Removes EMC directly into the internal buffer. Use for internal
	 * implementation of your tile
	 */
	protected void removeEMC(long toRemove)
	{
		currentEMC -= toRemove;
		if(currentEMC < 0)
			currentEMC = 0;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		if(currentEMC > maximumEMC)
			currentEMC = maximumEMC;
		nbt.setLong("EMC", Math.max(0, Math.min(currentEMC, maximumEMC)));
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("EMC", NBT.TAG_DOUBLE))
			currentEMC = Math.max(0, Math.min((long)nbt.getDouble("EMC"), maximumEMC));
		else
			currentEMC = Math.max(0, Math.min(nbt.getLong("EMC"), maximumEMC));
	}
	
	protected boolean hasMaxedEmc()
	{
		return getStoredEmc() >= getMaximumEmc();
	}
	
	class StackHandler extends ItemStackHandler
	{
		StackHandler(int size)
		{
			super(size);
		}
		
		@Override
		public void onContentsChanged(int slot)
		{
			TileSyncableTickableEMC.this.markDirty();
		}
	}
}