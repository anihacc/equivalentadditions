package tk.zeitheron.equivadditions.tiles;

import tk.zeitheron.equivadditions.InfoEA;
import tk.zeitheron.equivadditions.blocks.BlockEMCPipe;
import tk.zeitheron.equivadditions.pipes.IPipe;
import tk.zeitheron.equivadditions.pipes.emc.EMCAccessPoint;
import tk.zeitheron.equivadditions.pipes.emc.EMCPipe;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldUtil;

import moze_intel.projecte.api.tile.IEmcStorage;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEMCPipe extends TileSyncableTickable implements IPipe, ITooltipProviderHC
{
	public EMCPipe pipe;
	public BlockEMCPipe block;
	public EMCAccessPoint acessPoint;
	
	public boolean[] connects = new boolean[6];
	
	public TileEMCPipe(BlockEMCPipe block)
	{
		this.block = block;
	}
	
	public TileEMCPipe()
	{
	}
	
	@Override
	public void tick()
	{
		if(block == null)
		{
			block = WorldUtil.cast(world.getBlockState(pos).getBlock(), BlockEMCPipe.class);
			if(block == null)
			{
				world.removeTileEntity(pos);
				return;
			}
		}
		
		if(acessPoint == null)
			acessPoint = new EMCAccessPoint(getWorld(), getPos());
		
		if(pipe == null)
			pipe = block.pipe.apply(this);
		
		for(EnumFacing f : EnumFacing.VALUES)
		{
			TileEntity te = world.getTileEntity(pos.offset(f));
			connects[f.ordinal()] = te instanceof TileEMCPipe || te instanceof IEmcStorage;
		}
		
		pipe.update();
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("Block", block.getRegistryName().toString());
		if(pipe == null)
			return;
		NBTTagCompound n = new NBTTagCompound();
		pipe.write(n);
		nbt.setTag("Pipe", n);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		block = WorldUtil.cast(GameRegistry.findRegistry(Block.class).getValue(new ResourceLocation(nbt.getString("Block"))), BlockEMCPipe.class);
		if(block != null)
		{
			pipe = block.pipe.apply(this);
			pipe.read(nbt.getCompoundTag("Pipe"));
		}
	}
	
	public EMCPipe getRelPipe(EnumFacing to)
	{
		return getPipe(world, pos.offset(to));
	}
	
	public static EMCPipe getPipe(World world, BlockPos pos)
	{
		TileEMCPipe tep = WorldUtil.cast(world.getTileEntity(pos), TileEMCPipe.class);
		return tep != null ? tep.pipe : null;
	}
	
	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return connects[face.ordinal()];
	}
	
	@Override
	public ResourceLocation getTex()
	{
		return pipe != null ? pipe.getPipeTexture() : null;
	}
	
	@Override
	public void kill()
	{
		world.removeTileEntity(pos);
		if(pipe != null && pipe.grid != null)
			pipe.grid.onMajorGridChange();
	}
	
	private boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoEA.MOD_ID + ".transfer"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", block.transfer)).appendColor(TextFormatting.LIGHT_PURPLE));
		tip.append(new TranslationTooltipInfo("gui." + InfoEA.MOD_ID + ".emcpt"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoEA.MOD_ID + ".stored"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", Math.round(pipe.getBuffer().getEnergyStored()))).appendColor(TextFormatting.AQUA));
		tip.append(new StringTooltipInfo("EMC"));
	}
	
	@Override
	public BlockPos coordinates()
	{
		return getPos();
	}
	
	@Override
	public World world()
	{
		return getWorld();
	}
}